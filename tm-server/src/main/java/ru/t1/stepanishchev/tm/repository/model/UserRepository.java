package ru.t1.stepanishchev.tm.repository.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.stepanishchev.tm.api.repository.model.IUserRepository;
import ru.t1.stepanishchev.tm.model.User;

import java.util.List;

@Repository
@NoArgsConstructor
public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    @Override
    public List<User> findAll() {
        return entityManager.createQuery("FROM User", User.class).getResultList();
    }

    @Override
    @Nullable
    public User findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        @NotNull final String jpql = "SELECT m FROM User m WHERE m.id = :id";
        return entityManager.createQuery(jpql, User.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public User findOneByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) return null;
        @NotNull final String jpql = "SELECT m FROM User m WHERE m.login = :login";
        return entityManager.createQuery(jpql, User.class)
                .setParameter("login", login)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) return null;
        @NotNull final String jpql = "SELECT m FROM User m WHERE m.email = :email";
        return entityManager.createQuery(jpql, User.class)
                .setParameter("email", email)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public Boolean isLoginExist(@Nullable final String login) {
        return findOneByLogin(login) != null;
    }

    @Override
    public Boolean isEmailExist(@Nullable final String email) {
        return findOneByLogin(email) != null;
    }

    @Override
    public void clear() {
        @NotNull final String jpql = "DELETE FROM User";
        entityManager.createQuery(jpql).executeUpdate();
    }

}