package ru.t1.stepanishchev.tm.repository.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.stepanishchev.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.stepanishchev.tm.model.AbstractUserOwnedModel;

import javax.persistence.EntityManager;

@Repository
@NoArgsConstructor
public class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M>
        implements IUserOwnedRepository<M> {

    public AbstractUserOwnedRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

}