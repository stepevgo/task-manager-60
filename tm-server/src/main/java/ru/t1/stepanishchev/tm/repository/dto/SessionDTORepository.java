package ru.t1.stepanishchev.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.stepanishchev.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.stepanishchev.tm.dto.model.SessionDTO;

@Repository
@NoArgsConstructor
public class SessionDTORepository extends AbstractUserOwnedDTORepository<SessionDTO> implements ISessionDTORepository {

    @Nullable
    @Override
    public SessionDTO findOneById(@NotNull String id) {
        if (id.isEmpty()) return null;
        @NotNull final String jpql = "SELECT m FROM SessionDTO m WHERE m.id = :id";
        return entityManager.createQuery(jpql, SessionDTO.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public void clear(@NotNull String userId) {
        if (userId.isEmpty()) return;
        @NotNull final String jpql = "DELETE FROM SessionDTO m WHERE m.userId = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

}