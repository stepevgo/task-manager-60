package ru.t1.stepanishchev.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.stepanishchev.tm.api.repository.model.IAbstractRepository;
import ru.t1.stepanishchev.tm.api.service.model.IAbstractService;
import ru.t1.stepanishchev.tm.model.AbstractModel;

import java.util.Collection;

@Service
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractService<M extends AbstractModel, R extends IAbstractRepository<M>>
        implements IAbstractService<M> {

    @NotNull
    @Autowired
    protected IAbstractRepository<M> repository;

    @Override
    @Transactional
    public void add(@NotNull final M model) {
        repository.add(model);
    }

    @Override
    @Transactional
    public void addAll(@NotNull final Collection<M> models) {
        repository.addAll(models);
    }

    @Override
    @Transactional
    public void update(@NotNull final M model) {
        repository.update(model);
    }

    @Override
    @Transactional
    public void remove(@NotNull final M model) {
        repository.remove(model);
    }

}