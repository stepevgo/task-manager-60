package ru.t1.stepanishchev.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.stepanishchev.tm.api.repository.dto.IAbstractDTORepository;
import ru.t1.stepanishchev.tm.api.service.dto.IAbstractDTOService;
import ru.t1.stepanishchev.tm.dto.model.AbstractModelDTO;

import java.util.Collection;

@Service
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractDTOService<M extends AbstractModelDTO, R extends IAbstractDTORepository<M>>
        implements IAbstractDTOService<M> {

    @NotNull
    @Autowired
    protected IAbstractDTORepository<M> repository;

    @Override
    @Transactional
    public void add(@NotNull final M model) {
        repository.add(model);
    }

    @Override
    @Transactional
    public void addAll(@NotNull final Collection<M> models) {
        repository.addAll(models);
    }

    @Override
    @Transactional
    public void update(@NotNull final M model) {
        repository.update(model);
    }

    @Override
    @Transactional
    public void remove(@NotNull final M model) {
        repository.remove(model);
    }

}
