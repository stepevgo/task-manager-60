package ru.t1.stepanishchev.tm.listener.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.stepanishchev.tm.api.endpoint.IAuthEndpoint;
import ru.t1.stepanishchev.tm.api.endpoint.IUserEndpoint;
import ru.t1.stepanishchev.tm.listener.AbstractListener;

@Component
public abstract class AbstractUserListener extends AbstractListener {

    @Autowired
    protected IUserEndpoint userEndpoint;

    @Autowired
    protected IAuthEndpoint authEndpoint;

}