package ru.t1.stepanishchev.tm.exception.user;

public class AuthenticationException extends AbstractUserException {

    public AuthenticationException() {
        super("Authentication Error...");
    }

}